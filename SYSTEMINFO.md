# F5 BIG-IQ

Vendor: F5
Homepage: https://www.f5.com/

Product: BIG-IQ
Product Page: https://www.f5.com/products/big-ip-services/big-iq

## Introduction
We classify F5 BIG-IQ into the Network Services domain as F5 BIG-IQ centrally manages application delivery, traffic policy and security services. We also classify F5 BIG-IQ into the Data Center domain as it manages configurations, updates, and health monitoring of F5 devices.

"Easily control all your BIG-IP devices and services." 
"Simplify BIG-IP Portfolio Management."

## Why Integrate
The F5 BIG-IQ adapter from Itential is used to integrate the Itential Automation Platform (IAP) with F5 BIG-IQ to provide centralized management and orchestration for F5 networking devices. 

With this adapter you have the ability to perform operations with F5 BIG-IQ such as:
- Device
- Backup
- Upgrade
- Health

## Additional Product Documentation
The [API documents for F5 BIG-IQ](https://clouddocs.f5.com/products/big-iq/mgmt-api/v6.0.1/)
