
## 0.5.5 [10-15-2024]

* Changes made at 2024.10.14_20:27PM

See merge request itentialopensource/adapters/adapter-f5_bigiq!24

---

## 0.5.4 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-f5_bigiq!22

---

## 0.5.3 [08-14-2024]

* Changes made at 2024.08.14_18:39PM

See merge request itentialopensource/adapters/adapter-f5_bigiq!21

---

## 0.5.2 [08-07-2024]

* Changes made at 2024.08.06_19:51PM

See merge request itentialopensource/adapters/adapter-f5_bigiq!20

---

## 0.5.1 [07-09-2024]

* Added calls from License Management API

See merge request itentialopensource/adapters/adapter-f5_bigiq!19

---

## 0.5.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!17

---

## 0.4.9 [03-28-2024]

* Changes made at 2024.03.28_13:39PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!16

---

## 0.4.8 [03-21-2024]

* Changes made at 2024.03.21_14:05PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!15

---

## 0.4.7 [03-11-2024]

* Changes made at 2024.03.11_15:57PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!14

---

## 0.4.6 [03-08-2024]

* Adds call to delete script from BIG-IQ

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!13

---

## 0.4.5 [02-28-2024]

* Changes made at 2024.02.28_11:25AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!12

---

## 0.4.4 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!11

---

## 0.4.3 [12-22-2023]

* update axious

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!10

---

## 0.4.2 [12-14-2023]

* Added new Calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!9

---

## 0.4.1 [11-29-2023]

* Added new Calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!9

---

## 0.4.0 [11-08-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!6

---

## 0.3.5 [09-28-2023]

* Adds device upgrade methods for updating and retrieving device upgrades.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!8

---

## 0.3.4 [09-26-2023]

* Add device upgrade calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!7

---

## 0.3.3 [07-25-2023]

* Add default value to schema files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!5

---

## 0.3.2 [07-20-2023]

* Remove additional headers

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!4

---

## 0.3.1 [07-20-2023]

* Delete big-ip endpoints and add a big-iq task

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!3

---

## 0.3.0 [07-20-2023]

* Add missing endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-f5_bigiq!1

---

## 0.1.1 [07-20-2021]

- Initial Commit

See commit b3bb5ef

---
