## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for F5 BIG-IQ. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for F5 BIG-IQ.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the f5 BIG-IQ. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAlertRule(name, callback)</td>
    <td style="padding:15px">Get Alert Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/alert-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlertRule(name, body, callback)</td>
    <td style="padding:15px">Patch Alert Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/alert-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertRule(name, callback)</td>
    <td style="padding:15px">Delete Alert Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/alert-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlertRule(body, callback)</td>
    <td style="padding:15px">Post Alert Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/alert-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSet(id, callback)</td>
    <td style="padding:15px">Get Config Set</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/config-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplyTemplate(id, callback)</td>
    <td style="padding:15px">Get Apply Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/apply-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApplyTemplate(id, body, callback)</td>
    <td style="padding:15px">Patch Apply Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/apply-template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyTemplate(body, callback)</td>
    <td style="padding:15px">Post Apply Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/apply-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationsList(callback)</td>
    <td style="padding:15px">Get All Applications List</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/ap/query/v1/tenants/default/reports/AllApplicationsList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudEnvironment(id, callback)</td>
    <td style="padding:15px">Get Cloud Environment</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/environments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudEnvironments(callback)</td>
    <td style="padding:15px">List all Cloud Environments</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCloudEnvironment(id, body, callback)</td>
    <td style="padding:15px">Patch Cloud Enviornment</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/environments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudEnvironment(id, callback)</td>
    <td style="padding:15px">Delete Cloud Environment</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/environments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCloudEnvironment(body, callback)</td>
    <td style="padding:15px">Post Cloud Environment</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProvider(id, callback)</td>
    <td style="padding:15px">Get Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudProviders(callback)</td>
    <td style="padding:15px">List all Cloud Providers</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCloudProvider(id, body, callback)</td>
    <td style="padding:15px">Patch Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudProvider(id, callback)</td>
    <td style="padding:15px">Delete Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCloudProvider(body, callback)</td>
    <td style="padding:15px">Post Cloud Provider</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalTemplate(id, callback)</td>
    <td style="padding:15px">Get Global Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalTemplate(id, callback)</td>
    <td style="padding:15px">Delete Global Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGlobalTemplate(id, body, callback)</td>
    <td style="padding:15px">Patch Global Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGlobalTemplate(body, callback)</td>
    <td style="padding:15px">Post Global Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceScalingGroup(id, callback)</td>
    <td style="padding:15px">Get Service Scaling Group</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/service-scaling-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchServiceScalingGroup(id, body, callback)</td>
    <td style="padding:15px">Patch Service Scaling Group</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/cloud/service-scaling-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWorkflowRule(name, callback)</td>
    <td style="padding:15px">Patch Workflow Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/workflow-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowRule(name, callback)</td>
    <td style="padding:15px">Delete Workflow Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/workflow-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowRule(name, callback)</td>
    <td style="padding:15px">Get Workflow Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/workflow-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowRule(body, callback)</td>
    <td style="padding:15px">Post Workflow Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/policymgmt/workflow-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSuperDeploy(body, callback)</td>
    <td style="padding:15px">Post Super Deploy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/super-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSuperDeploy(id, callback)</td>
    <td style="padding:15px">Delete Super Deploy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/super-deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSuperDeploy(id, body, callback)</td>
    <td style="padding:15px">Patch Super Deploy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/super-deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuperDeploy(id, callback)</td>
    <td style="padding:15px">Get Super Deploy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/super-deploy{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPolicies(callback)</td>
    <td style="padding:15px">Get Access Policies</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/policy/access-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessProfile(id, callback)</td>
    <td style="padding:15px">Get Access Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/profile/access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveDirectoryServer(id, callback)</td>
    <td style="padding:15px">Get Active Directory Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/active-directory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActiveDirectoryServer(id, callback)</td>
    <td style="padding:15px">Delete Active Directory Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/active-directory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchActiveDirectoryServer(id, body, callback)</td>
    <td style="padding:15px">Patch Active Directory Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/active-directory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActiveDirectoryServer(body, callback)</td>
    <td style="padding:15px">Post Active Directory Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/active-directory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRevokeTokens(body, callback)</td>
    <td style="padding:15px">Post Revoke Tokens</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/revoke-tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRevokeTokens(id, callback)</td>
    <td style="padding:15px">Get Revoke Tokens</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/revoke-tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCRLDPServer(id, callback)</td>
    <td style="padding:15px">Get CRLDP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/crldp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCRLDPServer(id, callback)</td>
    <td style="padding:15px">Delete CRLDP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/crldp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCRLDPServer(id, body, callback)</td>
    <td style="padding:15px">Patch CRLDP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/crldp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCRLDPServer(id, body, callback)</td>
    <td style="padding:15px">Put CRLDP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/crldp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCRLDPServer(body, callback)</td>
    <td style="padding:15px">Post CRLDP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/crldp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientRateClass(id, callback)</td>
    <td style="padding:15px">Get Client Rate Class</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/clientRateClass/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientTrafficClassifier(id, callback)</td>
    <td style="padding:15px">Get Client Traffic Classifier</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/clientTrafficClassifier/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectivityProfile(id, callback)</td>
    <td style="padding:15px">Get Connectivity Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/profile/connectivity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDiscoveryImportController(id, callback)</td>
    <td style="padding:15px">Get Device Discovery Import Controller</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery-import-controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceDiscoveryImportController(id, callback)</td>
    <td style="padding:15px">Delete Device Discovery Import Controller</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery-import-controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceDiscoveryImportController(body, callback)</td>
    <td style="padding:15px">Post Device Discovery Import Controller</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery-import-controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHTTPServer(id, callback)</td>
    <td style="padding:15px">Get HTTP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/http/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putHTTPServer(id, body, callback)</td>
    <td style="padding:15px">Put HTTP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/http/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHTTPServer(id, callback)</td>
    <td style="padding:15px">Delete HTTP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/http/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchHTTPServer(id, body, callback)</td>
    <td style="padding:15px">Patch HTTP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/http/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHTTPServer(body, callback)</td>
    <td style="padding:15px">Post HTTP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv4LeasePool(id, callback)</td>
    <td style="padding:15px">Get IPv4 Lease Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/leasepool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6LeasePool(id, callback)</td>
    <td style="padding:15px">Get IPv6 Lease Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/ipv6Leasepool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKerberosKeyTabFile(id, callback)</td>
    <td style="padding:15px">Get Kerberos Key Tab File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/kerberos-keytab-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putKerberosKeyTabFile(id, body, callback)</td>
    <td style="padding:15px">Put Kerberos Key Tab File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/kerberos-keytab-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKerberosKeyTabFile(id, callback)</td>
    <td style="padding:15px">Delete Kerberos Key Tab File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/kerberos-keytab-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchKerberosKeyTabFile(id, body, callback)</td>
    <td style="padding:15px">Patch Kerberos Key Tab File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/kerberos-keytab-file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKerberosKeyTabFile(body, callback)</td>
    <td style="padding:15px">Post Kerberos Key Tab File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/kerberos-keytab-file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKillSession(body, callback)</td>
    <td style="padding:15px">Post Kill Session</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/kill-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKillSession(id, callback)</td>
    <td style="padding:15px">Get Kill Session</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/tasks/kill-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLDAPServer(id, callback)</td>
    <td style="padding:15px">Get LDAP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLDAPServer(id, body, callback)</td>
    <td style="padding:15px">Put LDAP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLDAPServer(id, callback)</td>
    <td style="padding:15px">Delete LDAP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLDAPServer(id, body, callback)</td>
    <td style="padding:15px">Patch LDAP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLDAPServer(body, callback)</td>
    <td style="padding:15px">Post LDAP Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAccess(id, callback)</td>
    <td style="padding:15px">Get Network Access</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkAccess(id, body, callback)</td>
    <td style="padding:15px">Put Network Access</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAccess(id, callback)</td>
    <td style="padding:15px">Delete Network Access</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworkAccess(id, body, callback)</td>
    <td style="padding:15px">Patch Network Access</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkAccess(body, callback)</td>
    <td style="padding:15px">Post Network Access</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAccessOptimizedApp(networkAccessId, id, callback)</td>
    <td style="padding:15px">Get Network Access Optimized App</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}/optimized-app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkAccessOptimizedApp(networkAccessId, id, body, callback)</td>
    <td style="padding:15px">Put Network Access Optimized App</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}/optimized-app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAccessOptimizedApp(networkAccessId, id, callback)</td>
    <td style="padding:15px">Delete Network Access Optimized App</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}/optimized-app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworkAccessOptimizedApp(networkAccessId, id, body, callback)</td>
    <td style="padding:15px">Patch Network Access Optimized App</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}/optimized-app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkAccessOptimizedApp(networkAccessId, body, callback)</td>
    <td style="padding:15px">Post Network Access Optimized App</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/resource/network-access/{pathv1}/optimized-app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPingAccessProfile(id, callback)</td>
    <td style="padding:15px">Get Ping Access Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/profile/ping-access/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileServerSSL(id, callback)</td>
    <td style="padding:15px">Get Profile Server SSL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/profile/server-ssl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRadiusServer(id, callback)</td>
    <td style="padding:15px">Get Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/radius/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRadiusServer(id, body, callback)</td>
    <td style="padding:15px">Put Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/radius/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRadiusServer(id, callback)</td>
    <td style="padding:15px">Delete Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/radius/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRadiusServer(id, body, callback)</td>
    <td style="padding:15px">Patch Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/radius/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRadiusServer(body, callback)</td>
    <td style="padding:15px">Post Radius Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/radius?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRewriteProfile(id, callback)</td>
    <td style="padding:15px">Get Rewrite Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/profile/rewrite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSAMLArtifactResolutionService(id, callback)</td>
    <td style="padding:15px">Get SAML Artifact Resolution Service</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/saml/artifact-resolution-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSAMLArtifactResolutionService(id, body, callback)</td>
    <td style="padding:15px">Put SAML Artifact Resolution Service</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/saml/artifact-resolution-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSAMLArtifactResolutionService(id, callback)</td>
    <td style="padding:15px">Delete SAML Artifact Resolution Service</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/saml/artifact-resolution-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSAMLArtifactResolutionService(id, body, callback)</td>
    <td style="padding:15px">Patch SAML Artifact Resolution Service</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/saml/artifact-resolution-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSAMLArtifactResolutionService(id, callback)</td>
    <td style="padding:15px">Post SAML Artifact Resolution Service</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/saml/artifact-resolution-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurIDConfigFile(securId, id, callback)</td>
    <td style="padding:15px">Get SecurID Config File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}/config-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecurIDConfigFile(securId, id, body, callback)</td>
    <td style="padding:15px">Put SecurID Config File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}/config-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurIDConfigFile(securId, id, callback)</td>
    <td style="padding:15px">Delete SecurID Config File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}/config-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSecurIDConfigFile(securId, id, callback)</td>
    <td style="padding:15px">Patch SecurID Config File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}/config-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecurIDConfigFile(securId, body, callback)</td>
    <td style="padding:15px">Post SecurID Config File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}/config-files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecureIDServer(id, callback)</td>
    <td style="padding:15px">Get SecurID Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecurIDServer(id, body, callback)</td>
    <td style="padding:15px">Put SecurID Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurIDServer(id, callback)</td>
    <td style="padding:15px">Delete SecurID Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSecurIDServer(id, body, callback)</td>
    <td style="padding:15px">Patch SecurID Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecurIDServer(body, callback)</td>
    <td style="padding:15px">Post SecurID Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/securid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNATPool(id, callback)</td>
    <td style="padding:15px">Get SNAT Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/snatpool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTacacsServer(id, callback)</td>
    <td style="padding:15px">Get TACACS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/tacacsplus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTacacsServer(id, body, callback)</td>
    <td style="padding:15px">Put TACACS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/tacacsplus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTacacsServer(id, callback)</td>
    <td style="padding:15px">Delete TACACS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/tacacsplus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTacacsServer(id, body, callback)</td>
    <td style="padding:15px">Patch TACACS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/tacacsplus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTACACSServer(body, callback)</td>
    <td style="padding:15px">Post TACACS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/aaa/tacacsplus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDefinedACL(id, callback)</td>
    <td style="padding:15px">Get User Defined ACL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUserDefinedACL(id, body, callback)</td>
    <td style="padding:15px">Put User Defined ACL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserDefinedACL(id, callback)</td>
    <td style="padding:15px">Delete User Defined ACL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUserDefinedACL(id, body, callback)</td>
    <td style="padding:15px">Patch User Defined ACL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserDefinedACL(body, callback)</td>
    <td style="padding:15px">Post User Defined ACL</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/acl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDIProfile(id, callback)</td>
    <td style="padding:15px">Get VDI Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/apm/profile/vdi/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualServer(id, callback)</td>
    <td style="padding:15px">Get Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualServer(id, body, callback)</td>
    <td style="padding:15px">Put Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualServer(id, body, callback)</td>
    <td style="padding:15px">Patch Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualServerProfile(virtualServerId, id, callback)</td>
    <td style="padding:15px">Get Virtual Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}/profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualServerProfile(virtualServerId, id, callback)</td>
    <td style="padding:15px">Delete Virtual Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}/profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualServerProfile(virtualServerId, body, callback)</td>
    <td style="padding:15px">Post Virtual Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/access/working-config/ltm/virtual/{pathv1}/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeclareManagementAuthorityCollection(callback)</td>
    <td style="padding:15px">Get Declare Management Authority Collection</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/declare-mgmt-authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeclareManagementAuthority(body, callback)</td>
    <td style="padding:15px">Post Declare Management Authority</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/declare-mgmt-authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeclareManagementAuthority(objectId, callback)</td>
    <td style="padding:15px">Get Declare Management Authority</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/declare-mgmt-authority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVirtualPools(callback)</td>
    <td style="padding:15px">Get All Virtual Pools</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualPool(objectId, callback)</td>
    <td style="padding:15px">Get Virtual Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLTMPoolMember(body, callback)</td>
    <td style="padding:15px">Post LTM Pool Member</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationServerNode(objectId, callback)</td>
    <td style="padding:15px">Delete Application Server Node</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPoolMembers(objectId, callback)</td>
    <td style="padding:15px">Get All Pool Members</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolMember(poolId, objectId, callback)</td>
    <td style="padding:15px">Get Pool Member</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/pool/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postADCSelfServiceTask(body, callback)</td>
    <td style="padding:15px">Post ADC Self Service Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/self-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllADCSelfServiceTasks(callback)</td>
    <td style="padding:15px">Get All ADC Self Service Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/self-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADCSelfServiceTask(objectId, callback)</td>
    <td style="padding:15px">Get ADC Self Service Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/tasks/self-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLTMApplicationServerNode(body, callback)</td>
    <td style="padding:15px">Post LTM Application Server Node</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLTMApplicationServerNodes(callback)</td>
    <td style="padding:15px">Get All LTM Application Server Nodes</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLTMApplicationServerNode(objectId, callback)</td>
    <td style="padding:15px">Get LTM Application Server Node</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLTMVirtualServer(body, callback)</td>
    <td style="padding:15px">Post LTM Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/virtual?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLTMVirtualServers(callback)</td>
    <td style="padding:15px">Get All LTM Virtual Servers</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/virtual?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLTMVirtualServer(objectId, callback)</td>
    <td style="padding:15px">Get LTM Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/ltm/virtual/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLCert(id, callback)</td>
    <td style="padding:15px">Get SSL Cert</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-cert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLCert(id, callback)</td>
    <td style="padding:15px">Delete SSL Cert</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-cert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLCertRevocationList(id, callback)</td>
    <td style="padding:15px">Delete SSL Cert Revocation List</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-crl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">geleteSSLCertRevocationList(id, callback)</td>
    <td style="padding:15px">Get SSL Cert Revocation List</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-crl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLCertificateSigningRequest(id, callback)</td>
    <td style="padding:15px">Get SSL Certificate Signing Request</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-csr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLCertificateSigningRequest(id, callback)</td>
    <td style="padding:15px">Delete SSL Certificate Signing Request</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-csr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSLKey(id, callback)</td>
    <td style="padding:15px">Get SSL Key</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-key/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSLKey(id, callback)</td>
    <td style="padding:15px">Delete SSL Key</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/adc-core/working-config/sys/file/ssl-key/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postASMPolicy(body, callback)</td>
    <td style="padding:15px">Post ASM Policy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/asm/working-config/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllASMPolicies(callback)</td>
    <td style="padding:15px">Get All ASM Policies</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/asm/working-config/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASMPolicy(objectId, callback)</td>
    <td style="padding:15px">Get ASM Policy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/asm/working-config/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllASMSignatures(callback)</td>
    <td style="padding:15px">Get All ASM Signatures</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/asm/working-config/signatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASMSignature(objectId, callback)</td>
    <td style="padding:15px">Get ASM Signature</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/asm/working-config/signatures/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceIPPool(id, callback)</td>
    <td style="padding:15px">Get Device IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ip-pool/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceIPPools(id, body, callback)</td>
    <td style="padding:15px">Patch Device IP Pools</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ip-pool/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceIPPool(id, callback)</td>
    <td style="padding:15px">Delete Device IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ip-pool/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDeviceIPPools(callback)</td>
    <td style="padding:15px">Get all IP Pools</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ip-pool/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceIPPool(body, callback)</td>
    <td style="padding:15px">Post Device IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ip-pool/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceDiscovery(body, callback)</td>
    <td style="padding:15px">Post Device Discovery</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceDiscoveryTasks(callback)</td>
    <td style="padding:15px">Get All Device Discovery Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDiscoveryTask(objectId, callback)</td>
    <td style="padding:15px">Get Device Discovery Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performDeviceDiscoveryTask(objectId, body, callback)</td>
    <td style="padding:15px">Perform Device Discovery Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTrustTask(body, callback)</td>
    <td style="padding:15px">Post Device Trust Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-trust?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceTrustTasks(callback)</td>
    <td style="padding:15px">Get All Device Trust Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-trust?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTrustTask(objectId, callback)</td>
    <td style="padding:15px">Get Device Trust Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-trust/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceReferenceQuery(body, callback)</td>
    <td style="padding:15px">Post Device Reference Query</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/shared/device-reference-query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplate(id, callback)</td>
    <td style="padding:15px">Get Device Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceTemplate(id, body, callback)</td>
    <td style="padding:15px">Patch Device Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplate(id, callback)</td>
    <td style="padding:15px">Delete Device Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplate(body, callback)</td>
    <td style="padding:15px">Post Device Template</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSDataCenter(id, callback)</td>
    <td style="padding:15px">Get DNS Data Center</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/datacenter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSDataCenter(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Data Center</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/datacenter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSDataCenter(id, callback)</td>
    <td style="padding:15px">Delete DNS Data Center</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/datacenter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSDataCenter(body, callback)</td>
    <td style="padding:15px">Post DNS Data Center</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/datacenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSiRule(id, callback)</td>
    <td style="padding:15px">Get DNS iRule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSiRule(id, callback)</td>
    <td style="padding:15px">Delete DNS iRule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSiRule(id, body, callback)</td>
    <td style="padding:15px">Patch DNS iRule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/rule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSiRule(body, callback)</td>
    <td style="padding:15px">Post DNS iRule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolA(id, callback)</td>
    <td style="padding:15px">Get DNS Pool A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolA(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolA(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolA(body, callback)</td>
    <td style="padding:15px">Post DNS Pool A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolAAAA(id, callback)</td>
    <td style="padding:15px">Get DNS Pool AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolAAAA(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolAAAA(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolAAAA(body, callback)</td>
    <td style="padding:15px">Post DNS Pool AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolCNAME(id, callback)</td>
    <td style="padding:15px">Get DNS Pool CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolCNAME(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolCNAME(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolCNAME(body, callback)</td>
    <td style="padding:15px">Post DNS Pool CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberA(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMemberA(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberA(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberA(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/a/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberAAAA(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMember(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberAAAA(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberAAAA(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/aaaa/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberCNAME(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/cname/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMemberCNAME(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/cname/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberCNAME(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/cname/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberCNAME(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/cname/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberMX(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMemberMX(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberMX(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberMX(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberNAPTR(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMemberNAPTR(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberNAPTR(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberNAPTR(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMemberSRV(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Get DNS Pool Member SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMemberSRV(poolId, poolMemberId, callback)</td>
    <td style="padding:15px">Delete DNS Pool Member SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMemberSRV(poolId, poolMemberId, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool Member SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMemberSRV(poolId, body, callback)</td>
    <td style="padding:15px">Post DNS Pool Member SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolMX(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolMX(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolMX(id, callback)</td>
    <td style="padding:15px">Get DNS Pool MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolMX(body, callback)</td>
    <td style="padding:15px">Post DNS Pool MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolNAPTR(id, callback)</td>
    <td style="padding:15px">Get DNS Pool NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolNAPTR(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolNAPTR(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSPoolSRV(id, callback)</td>
    <td style="padding:15px">Get DNS Pool SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSPoolSRV(id, callback)</td>
    <td style="padding:15px">Delete DNS Pool SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSPoolSRV(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Pool SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolNAPTR(body, callback)</td>
    <td style="padding:15px">Post DNS Pool NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/naptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSPoolSRV(body, callback)</td>
    <td style="padding:15px">Post DNS Pool SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/pool/srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSRegion(id, callback)</td>
    <td style="padding:15px">Get DNS Region</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/region/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSRegion(id, callback)</td>
    <td style="padding:15px">Delete DNS Region</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/region/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSRegion(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Region</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/region/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSRegion(body, callback)</td>
    <td style="padding:15px">Post DNS Region</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/region?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSServer(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServer(id, callback)</td>
    <td style="padding:15px">Get DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServer(id, callback)</td>
    <td style="padding:15px">Delete DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSServer(body, callback)</td>
    <td style="padding:15px">Post DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerDevice(serverId, deviceId, callback)</td>
    <td style="padding:15px">Get DNS Server Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServerDevice(serverId, deviceId, callback)</td>
    <td style="padding:15px">Delete DNS Server Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSServerDevice(serverId, deviceId, body, callback)</td>
    <td style="padding:15px">Patch DNS Server Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSServerDevice(serverId, body, callback)</td>
    <td style="padding:15px">Post DNS Server Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSTopology(id, callback)</td>
    <td style="padding:15px">Get DNS Topology</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSTopolgoy(id, callback)</td>
    <td style="padding:15px">Delete DNS Topology</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSTopology(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Topology</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSTopology(body, callback)</td>
    <td style="padding:15px">Post DNS Topology</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSVirtualServer(serverId, virtualServerId, body, callback)</td>
    <td style="padding:15px">Patch DNS Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/virtual-servers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSVirtualServer(serverId, virtualServerId, callback)</td>
    <td style="padding:15px">Get DNS Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/virtual-servers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSVirtualServer(serverId, virtualServerId, callback)</td>
    <td style="padding:15px">Delete DNS Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/virtual-servers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSVirtualServer(serverId, body, callback)</td>
    <td style="padding:15px">Post DNS Virtual Server</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/server/{pathv1}/virtual-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPA(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPA(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPA(id, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPA(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP A</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPAAAA(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPAAAA(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPAAAA(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPAAAA(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP AAAA</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPCNAME(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPCNAME(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPCNAME(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPMX(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPMX(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPMX(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPNAPTR(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPNAPTR(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSWideIPSRV(id, callback)</td>
    <td style="padding:15px">Get DNS Wide IP SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSWideIPSRV(id, callback)</td>
    <td style="padding:15px">Delete DNS Wide IP SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPCNAME(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP CNAME</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPMX(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP MX</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPNAPTR(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/naptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDNSWideIPSRV(body, callback)</td>
    <td style="padding:15px">Post DNS Wide IP SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/wideip/srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPSRV(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP SRV</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/srv/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDNSWideIPNAPTR(id, body, callback)</td>
    <td style="padding:15px">Patch DNS Wide IP NAPTR</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/dns/working-config/naptr/mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRetrieveFirewallExpanedRules(body, callback)</td>
    <td style="padding:15px">Post Retrieve Firewall Expanded Rules</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/utility/expanded-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallRuleDeclareManagementAuthorityTask(body, callback)</td>
    <td style="padding:15px">Post Firewall Rule Declare Management Authority Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/declare-mgmt-authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFirewallRuleDeclareManagementAuthorityTask(callback)</td>
    <td style="padding:15px">Get All Firewall Rule Declare Management Authority Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/declare-mgmt-authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallRuleDeclareManagementAuthorityTask(objectId, callback)</td>
    <td style="padding:15px">Get Firewall Rule Declare Management Authority Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/declare-mgmt-authority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFirewalls(callback)</td>
    <td style="padding:15px">Get All Firewalls</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/firewalls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewall(objectId, callback)</td>
    <td style="padding:15px">Get Firewall</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/firewalls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFirewall(objectId, body, callback)</td>
    <td style="padding:15px">Patch Firewall</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/firewalls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFirewallDeployConfiguration(callback)</td>
    <td style="padding:15px">Get All Firewall Deploy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/deploy-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallDeployConfiguration(objectId, callback)</td>
    <td style="padding:15px">Get Firewall Deploy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/deploy-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallDeployConfiguration(body, callback)</td>
    <td style="padding:15px">Post Firewall Deploy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewall/tasks/deploy-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFirewallPolicies(callback)</td>
    <td style="padding:15px">Get All Firewall Policies</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicy(objectId, callback)</td>
    <td style="padding:15px">Get Firewall Policy</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFirewallPolicyRules(objectId, callback)</td>
    <td style="padding:15px">Get All Firewall Policy Rules</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/policies/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallPolicyRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Get Firewall Policy Rule</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/firewalls/working-config/policies/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResolveMachineId(filter, callback)</td>
    <td style="padding:15px">Resolve machineId</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/machineid-resolver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleQkview(callback)</td>
    <td style="padding:15px">EXAMPLE - Qkview</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveQkviewTasks(callback)</td>
    <td style="padding:15px">Retrieve Qkview Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateaQkview(body, callback)</td>
    <td style="padding:15px">Generate a Qkview</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryBIGIQQkviewTask(bIGIQQKVIEWTASKID, callback)</td>
    <td style="padding:15px">Query BIG-IQ Qkview Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBIGIQQkview(bIGIQQKVIEWTASKID, callback)</td>
    <td style="padding:15px">Delete BIG-IQ Qkview</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryBIGIPQkviewTask(bIGIPQKVIEWTASKID, callback)</td>
    <td style="padding:15px">Query BIGIP Qkview Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteaBIGIQQkview(bIGIPQKVIEWTASKID, callback)</td>
    <td style="padding:15px">Delete a BIGIP Qkview</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/qkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBIGIPiHealthReport(callback)</td>
    <td style="padding:15px">EXAMPLE - BIG-IP iHealth Report</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/reports/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBIGIPiHealthUploads(callback)</td>
    <td style="padding:15px">EXAMPLE - BIG-IP iHealth Uploads</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/uploads/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBIGIPiHealthCredentials(callback)</td>
    <td style="padding:15px">EXAMPLE - BIG-IP iHealth Credentials</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/credentials/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBIGIPiHealthConfig(callback)</td>
    <td style="padding:15px">EXAMPLE - BIG-IP iHealth Config</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/config/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPiHealthReports(callback)</td>
    <td style="padding:15px">Retrieve BIG-IP iHealth Reports</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateBIGIPiHealthReport(body, callback)</td>
    <td style="padding:15px">Generate BIG-IP iHealth Report</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPiHealthCredentials(filter, callback)</td>
    <td style="padding:15px">Retrieve BIG-IP iHealth Credentials</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTestBIGIPiHealthCredentials(body, callback)</td>
    <td style="padding:15px">Create Test BIG-IP iHealth Credentials</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPiHealthConfig(callback)</td>
    <td style="padding:15px">Retrieve BIG-IP iHealth Config</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBIGIPiHealthConfig(body, callback)</td>
    <td style="padding:15px">Update BIG-IP iHealth Config</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPiHealthUploads(callback)</td>
    <td style="padding:15px">Retrieve BIG-IP iHealth Uploads</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/uploads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBIGIPiHealthUploadTask(body, callback)</td>
    <td style="padding:15px">Create BIG-IP iHealth Upload Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/uploads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryUploadTaskStatus(iHealthUploadTaskId, callback)</td>
    <td style="padding:15px">Query Upload Task Status</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/uploads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTestBIGIPiHealthCredentials(credentialsId, body, callback)</td>
    <td style="padding:15px">Modify Test BIG-IP iHealth Credentials</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTestBIGIPiHealthCredentials(credentialsId, callback)</td>
    <td style="padding:15px">Delete Test BIG-IP iHealth Credentials</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/ihealth/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyProperSystemStatus(callback)</td>
    <td style="padding:15px">Verify Proper System Status</td>
    <td style="padding:15px">{base_path}/{version}/info/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyProperHAStatus(callback)</td>
    <td style="padding:15px">Verify Proper HA Status</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/failover-state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBackupRestoreTask(callback)</td>
    <td style="padding:15px">EXAMPLE - Backup/Restore Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/backup-restore/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveallBackupTasks(callback)</td>
    <td style="padding:15px">Retrieve all Backup Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/backup-restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateBackupofDevice(body, callback)</td>
    <td style="padding:15px">Generate Backup of Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/backup-restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryBackupTaskStatus(backupTaskId, callback)</td>
    <td style="padding:15px">Query Backup Task Status</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/backup-restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBackupFile(backupTaskId, callback)</td>
    <td style="padding:15px">Delete Backup File</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/system/backup-restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateBackupFileDownloadPathHash(body, callback)</td>
    <td style="padding:15px">Generate Backup File Download Path Hash</td>
    <td style="padding:15px">{base_path}/{version}/info/filedownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadBackupFileusingPathHash(pathHash, callback)</td>
    <td style="padding:15px">Download Backup File using Path Hash</td>
    <td style="padding:15px">{base_path}/{version}/info/filedownload/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExampleBIGIPscripts(callback)</td>
    <td style="padding:15px">EXAMPLE - BIG-IP scripts</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-scripts/example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveallBIGIPscripts(callback)</td>
    <td style="padding:15px">Retrieve all BIG-IP scripts</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createaBIGIPscript(body, callback)</td>
    <td style="padding:15px">Create a BIG-IP script</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeaBIGIPscript(body, callback)</td>
    <td style="padding:15px">Execute a BIG-IP script</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-script-execution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryBIGIPscriptstatus(scriptTaskId, callback)</td>
    <td style="padding:15px">Query BIG-IP script status</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-script-execution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBIGIPscript(scriptId, callback)</td>
    <td style="padding:15px">Delete BIG-IP script</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/user-scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveallSoftwareImages(filter, orderby, callback)</td>
    <td style="padding:15px">Retrieve all Software Images</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/autodeploy/software-images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveallBIGIPsandimageslots(filter, callback)</td>
    <td style="padding:15px">Retrieve all BIG-IPs and image slots</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/resolver/device-groups/cm-bigip-allBigIpDevices/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievaallUpgradeTasks(callback)</td>
    <td style="padding:15px">Retrieve all upgrade tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performSoftwareUpgrade(body, callback)</td>
    <td style="padding:15px">Perform software upgrade</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPdevices(select, filter, callback)</td>
    <td style="padding:15px">Retrieve BIG-IP devices</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/resolver/device-groups/cm-bigip-allDevices/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDeviceGroups(filter, orderby, select, callback)</td>
    <td style="padding:15px">Retrieve Device Groups</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/resolver/device-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPHealthSummary(instanceUuid, callback)</td>
    <td style="padding:15px">Retrieve BIG-IP Health Summary</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/resolver/device-groups/cm-bigip-allDevices/devices/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveBIGIPDeviceImportTasks(filter, callback)</td>
    <td style="padding:15px">Retrieve BIG-IP Device Import Tasks</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performBIGIPDeviceImportusingmachineId(body, callback)</td>
    <td style="padding:15px">Perform BIG-IP Device Import using machineId</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performBIGIPDeviceImportusingImportTask(deviceImportId, body, callback)</td>
    <td style="padding:15px">Perform BIG-IP Device Import using Import Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-import/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorImportTask(deviceImportId, callback)</td>
    <td style="padding:15px">Monitor Import Task</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-import/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDevicebyDeviceReference(uuid, callback)</td>
    <td style="padding:15px">Query Device by Device Reference</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/shared/resolver/device-groups/cm-bigip-allBigIpDevices/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDSCGroup(filter, callback)</td>
    <td style="padding:15px">Query DSC Group</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/dsc-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDSCGroupByGroupId(groupId, callback)</td>
    <td style="padding:15px">Query DSC Group by group id</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/dsc-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootDevice(body, callback)</td>
    <td style="padding:15px">Reboot Device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/tasks/device-item-deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceServices(body, callback)</td>
    <td style="padding:15px">Remove Device Services</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-remove-mgmt-authority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoveDeviceServicesStatusById(id, callback)</td>
    <td style="padding:15px">Get Remove Device Services Status by Id</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-remove-mgmt-authority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceTrust(body, callback)</td>
    <td style="padding:15px">Remove Device Trust</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-remove-trust?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoveDeviceTrustStatusById(id, callback)</td>
    <td style="padding:15px">Get Remove Device Trust Status By Id</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/global/tasks/device-remove-trust/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceUpgrade(body, callback)</td>
    <td style="padding:15px">Update Device Upgrade</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/upgrade-backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUpgradeInstances(callback)</td>
    <td style="padding:15px">Get All Upgrade Instances</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/upgrade-backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeInstancesById(upgradeTaskId, callback)</td>
    <td style="padding:15px">Get Upgrade Instance By Id</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/upgrade-backups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initialLicenseActivation(body, callback)</td>
    <td style="padding:15px">Activate a license</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/initial-activation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pollInitialLicenseActivation(registrationKey, callback)</td>
    <td style="padding:15px">Poll license activation status.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/initial-activation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">completeInitialActivation(registrationKey, body, callback)</td>
    <td style="padding:15px">Complete or retry an activation.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/initial-activation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeFailedInitialActivation(registrationKey, callback)</td>
    <td style="padding:15px">Remove failed activation.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/initial-activation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseReference(uuid, callback)</td>
    <td style="padding:15px">Get license reference.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactivateLicense(uuid, body, callback)</td>
    <td style="padding:15px">Reactivate or complete activation of purchased pool license.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchasedPoolLicense(uuid, callback)</td>
    <td style="padding:15px">Delete a purchased pool license.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignOrRevokeLicense(body, callback)</td>
    <td style="padding:15px">Assign or revoke a pool license to a managed device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/tasks/licensing/pool/member-management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pollTaskForCompletion(uuid, callback)</td>
    <td style="padding:15px">Pool assign or revoke license task for completion.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/tasks/licensing/pool/member-management/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">viewOutstandingAssignments(callback)</td>
    <td style="padding:15px">View outstanding assignments.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryExistingPurchasedLicenses(callback)</td>
    <td style="padding:15px">Query existing purchased pool licenses.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateNewPurchasedPoolLicense(body, callback)</td>
    <td style="padding:15px">Activate a purchased pool license</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryPurchasedPoolLicenseAssignments(uuid, callback)</td>
    <td style="padding:15px">Query assignments for a purchased pool license.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignPurchasedPoolLicenseMembers(uuid, body, callback)</td>
    <td style="padding:15px">Assign license to managed device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryPurchasedPoolLicenseAssignmentStatus(uuid, memberUuid, callback)</td>
    <td style="padding:15px">Query license assignment status.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshPurchasedPoolLicenseOnBigIPDevice(uuid, memberUuid, body, callback)</td>
    <td style="padding:15px">Refresh the license on a BIG-IP device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeAPurchasedPoolLicense(uuid, memberUuid, callback)</td>
    <td style="padding:15px">Revoke a license from a device.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/purchased-pool/licenses/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryExistingRegKeyPools(callback)</td>
    <td style="padding:15px">Query existing RegKey Pools.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRegKeyPool(body, callback)</td>
    <td style="padding:15px">Create a RegKey Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRegKeyPool(id, body, callback)</td>
    <td style="padding:15px">Update a RegKey Pool to change name and/or description</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeRegKeyPool(id, callback)</td>
    <td style="padding:15px">Remove a RegKey Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryExistingLicenseKeysForRegKeyPool(id, callback)</td>
    <td style="padding:15px">Query existing license keys for a RegKey Pool.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLicenseKey(id, body, callback)</td>
    <td style="padding:15px">Add a license key</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pollForRegKeyActivationStatus(id, regKey, callback)</td>
    <td style="padding:15px">Poll to get status</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">completeRegKeyActivation(id, regKey, body, callback)</td>
    <td style="padding:15px">Complete or retry reg key activiation. May also reactivate a license with add-on keys.</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRegistrationKey(id, regKey, callback)</td>
    <td style="padding:15px">Delete a Registration key</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegistrationKeyAssignment(id, regKey, callback)</td>
    <td style="padding:15px">Get the assignment for a registration key in a RegKey Pool</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRegistrationKeyToDevice(id, regKey, body, callback)</td>
    <td style="padding:15px">Assign a license to a managed or unmanaged device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegistrationKeyAssignmentStatus(id, regKey, memberUuid, callback)</td>
    <td style="padding:15px">Get the status of a new registration key assignment</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}/members/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokedRegistrationKeyFromDevice(id, regKey, memberUuid, body, callback)</td>
    <td style="padding:15px">Revoke a license from a managed or unmanaged device</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/pool/regkey/licenses/{pathv1}/offerings/{pathv2}/members/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setLicenseText(body, callback)</td>
    <td style="padding:15px">Set the license text</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/setup/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseText(callback)</td>
    <td style="padding:15px">Get the license text</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/setup/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUtilityBillingLicenseReport(body, callback)</td>
    <td style="padding:15px">Create utility billing license report</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/tasks/licensing/utility-billing-reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUtilityBillingReportStatus(id, callback)</td>
    <td style="padding:15px">View the status of a utility billing report</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/tasks/licensing/utility-billing-reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadUtilityBillingReport(report, callback)</td>
    <td style="padding:15px">Download a utility billing report</td>
    <td style="padding:15px">{base_path}/{version}/mgmt/cm/device/licensing/license-reports-download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
