# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the F5BigIQ System. The API that was used to build the adapter for F5BigIQ is usually available in the report directory of this adapter. The adapter utilizes the F5BigIQ API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The F5 BIG-IQ adapter from Itential is used to integrate the Itential Automation Platform (IAP) with F5 BIG-IQ to provide centralized management and orchestration for F5 networking devices. 

With this adapter you have the ability to perform operations with F5 BIG-IQ such as:
- Device
- Backup
- Upgrade
- Health

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
